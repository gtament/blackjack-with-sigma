package com.sigmaukraine.blackjack.client;

import akka.actor.ActorSystem;
import akka.actor.TypedActor;
import akka.actor.TypedProps;
import akka.japi.Creator;
import akka.japi.Option;
import com.sigmaukraine.blackjack.common.msgs.GameroomInfo;
import com.sigmaukraine.blackjack.common.msgs.PlayerInfo;

import java.util.ArrayList;
import java.util.List;

public class ClientServerManager {
    private static ClientServerManager instance;

    private ClientInt client;
    private boolean connected;

    public static final String SERVER_URL = "akka.tcp://BlackjackServer@127.0.0.1:2480/user/connaccept";

    private ClientServerManager() {
        final ActorSystem system = ActorSystem.create("SC");
        client =
                TypedActor.get(system).typedActorOf(
                        new TypedProps<TypedClient>(ClientInt.class,
                                new Creator<TypedClient>() {
                                    public TypedClient create() {
                                        return new TypedClient(SERVER_URL);
                                    }
                                }),
                        "name");
    }

    public static synchronized ClientServerManager getInstance() {
        if(instance == null) {
            instance = new ClientServerManager();
        }
        return instance;
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean establishConnection() {
        Option<Boolean> result = client.establishConnection();
        connected = result.get().booleanValue();
        return connected;
    }

    public PlayerInfo signIn(String username, String password) {
        Option<PlayerInfo> result = client.signIn(username, password);
        if(result!=null)
            return result.get();
        return null;
    }

    public PlayerInfo signUp(String username, String password) {
        Option<PlayerInfo> result = client.signUp(username, password);
        if(result!=null)
            return result.get();
        return null;
    }

    public List<PlayerInfo> getLeaderboard(int from, int count){
        Option<List<PlayerInfo>> board = client.getLeaderboard(from, count);
        return board.get();
    }

    public List<GameroomInfo> getGamerooms(){
        Option<List<GameroomInfo>> rooms = client.getGamerooms();
        return rooms.get();
    }

    public List<String> joinGameroom(GameroomInfo info){
        Option<List<String>> result = client.joinGameroom(info);
        if(result!=null)
            return result.get();
        return new ArrayList<>();
    }

    public boolean createGameroom(GameroomInfo info){
        Option<Boolean> result = client.createGameroom(info);
        return result.get().booleanValue();
    }

    public int getPlayerRank(){
        Option<Integer> result = client.getPlayerRank();
        return result.get().intValue();
    }

    public void startRound(){
        client.startRound();
    }

    public void signOut() {
        connected = false;
        client.signOut();
    }

    public void setGamelogicEventsHandler(GamelogicEvents handler){
        client.setGamelogicEventsHandler(handler);
    }
}
