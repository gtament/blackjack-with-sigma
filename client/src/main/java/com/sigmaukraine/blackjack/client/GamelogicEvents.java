package com.sigmaukraine.blackjack.client;

import com.sigmaukraine.blackjack.common.cards.Card;

public interface GamelogicEvents {
    long onRequestBet();
    boolean onRequestHitOrStand();
    void onDealersCard(Card card, boolean isShowed);
    void onPlayersCard(Card card, String playerName);
    void onOpenDealersCard();
    void onSomebodyBet(long bet, String name);
    void onSomebodyHOS(boolean hos, String name);
    void onRoundResult(long winning, String name);
}
