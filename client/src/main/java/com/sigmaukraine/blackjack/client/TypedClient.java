package com.sigmaukraine.blackjack.client;

import akka.actor.*;
import akka.japi.Option;
import akka.pattern.Patterns;
import com.sigmaukraine.blackjack.common.msgs.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TypedClient implements ClientInt, TypedActor.Receiver {
    private ActorRef session = null;
    private ActorSelection serverBefore;
    private ActorRef server;
    private String remote;
    private GamelogicEvents glehandler;

    private static final Logger LOG = LogManager.getLogger(TypedClient.class.getName());

    public TypedClient (String remote) {
        this.remote=remote;
    }

    @Override
    public Option<PlayerInfo> signIn(String username, String password) {
        Duration timeout = Duration.create(1000, TimeUnit.MILLISECONDS);
        Future<Object> future = Patterns.ask(session, new AuthInfo(username, password), timeout.length());
        AuthResult result = null;
        try {
            result = (AuthResult) Await.result(future, timeout);
        }
        catch (Exception e) {
            LOG.catching(e);
        }
        if(result instanceof AuthSuccess)
            return Option.some(((AuthSuccess) result).getInfo());
        return null;
    }

    @Override
    public Option<PlayerInfo> signUp(String username, String password) {
        Duration timeout = Duration.create(1000, TimeUnit.MILLISECONDS);
        Future<Object> future = Patterns.ask(session, new AuthInfo(username, password, true), timeout.length());
        AuthResult result = null;
        try {
            result = (AuthResult) Await.result(future, timeout);
        }
        catch (Exception e){
            LOG.catching(e);
        }
        if(result instanceof AuthSuccess)
            return Option.some(((AuthSuccess) result).getInfo());
        return null;
    }

    @Override
    public Option<Boolean> establishConnection() {
        serverBefore = TypedActor.context().actorSelection(remote);
        Duration timeout = Duration.create(1000, TimeUnit.MILLISECONDS);
        Future<Object> connectToServer = Patterns.ask(serverBefore, new Identify(remote), timeout.length());
        ActorIdentity temp = null;
        try {
            temp = (ActorIdentity) Await.result(connectToServer, timeout);
        }
        catch (Exception e) {
            LOG.catching(e);
        }
        if(temp==null)
            return Option.some(Boolean.valueOf(false));

        server = temp.getRef();

        Future<Object> getSession = Patterns.ask(server, new NewConn(), timeout.length());
        try {
            session = (ActorRef) Await.result(getSession, timeout);
        }
        catch (Exception e) {
            LOG.catching(e);
        }
        if(session==null)
            return Option.some(new Boolean(false));
        return Option.some(new Boolean(true));
    }

    @Override
    public Option<List<PlayerInfo>> getLeaderboard(int from, int count){
        Duration timeout = Duration.create(1000, TimeUnit.MILLISECONDS);
        Future<Object> future = Patterns.ask(session, new GetLeaderboard(from, count), timeout.length());
        LeaderboardMsg board = null;
        try {
            board = (LeaderboardMsg) Await.result(future, timeout);
        }
        catch(Exception e) {
            LOG.catching(e);
        }
        if(board!=null) {
            return Option.some(board.getBoard());
        }
        return null;
    }

    @Override
    public Option<List<GameroomInfo>> getGamerooms(){
        Duration timeout = Duration.create(1000, TimeUnit.MILLISECONDS);
        Future<Object> future = Patterns.ask(session, new GetGamerooms(), timeout.length());
        GameroomsMsg rooms = null;
        try{
            rooms = (GameroomsMsg) Await.result(future, timeout);
        }
        catch(Exception e){
            LOG.catching(e);
        }
        if(rooms!=null){
            return Option.some(rooms.getRooms());
        }
        return null;
    }

    @Override
    public void onReceive(Object msg, ActorRef sender) {
        LOG.info(msg.toString());
        if(glehandler!=null) {
            if (msg instanceof RequestBet) {
                sender.tell(new BetAnswer(glehandler.onRequestBet()), getSelf());
                return;
            }
            if (msg instanceof RequestHitOrStand) {
                sender.tell(new HOSAnswer(glehandler.onRequestHitOrStand()), getSelf());
                return;
            }
            if (msg instanceof OpenDealersCardsMsg) {
                glehandler.onOpenDealersCard();
                return;
            }
            if (msg instanceof PlayersCardMsg) {
                glehandler.onPlayersCard(((PlayersCardMsg) msg).getCard(), ((PlayersCardMsg) msg).getName());
                return;
            }
            if (msg instanceof DealersCardMsg) {
                glehandler.onDealersCard(((DealersCardMsg) msg).getCard(), ((DealersCardMsg) msg).isShowed());
                return;
            }
            if (msg instanceof SomebodyBet) {
                glehandler.onSomebodyBet(((SomebodyBet) msg).getBet(), ((SomebodyBet) msg).getName());
                return;
            }
            if (msg instanceof SomebodyHOS) {
                glehandler.onSomebodyHOS(((SomebodyHOS) msg).getHOS(), ((SomebodyHOS) msg).getName());
                return;
            }
            if (msg instanceof RoundResultMsg){
                glehandler.onRoundResult(((RoundResultMsg) msg).getWinning(), ((RoundResultMsg) msg).getName());
                return;
            }
        }
    }

    @Override
    public Option<List<String>> joinGameroom(GameroomInfo info) {
        Duration timeout = Duration.create(1000, TimeUnit.MILLISECONDS);
        JoinGameroomMsg msg = new JoinGameroomMsg(info);
        Future<Object> future = Patterns.ask(session, msg, timeout.length());
        JoinResult result = null;
        try{
            result = (JoinResult) Await.result(future, timeout);
        }
        catch (Exception e) {
            LOG.catching(e);
        }
        if(result instanceof JoinSuccess)
            return Option.some(((JoinSuccess) result).getPlayers());
        return null;
    }

    @Override
    public Option<Boolean> createGameroom(GameroomInfo info) {
        Duration timeout = Duration.create(1000, TimeUnit.MILLISECONDS);
        Future<Object> future = Patterns.ask(session, new CreateGameroomMsg(info), timeout.length());
        CreateResult result = null;
        try{
            result = (CreateResult) Await.result(future, timeout);
        }
        catch (Exception e){
            LOG.catching(e);
        }
        if(result instanceof CreateSuccess)
            return Option.some(Boolean.valueOf(true));
        return Option.some(Boolean.valueOf(false));
    }

    @Override
    public Option<Integer> getPlayerRank() {
        Duration timeout = Duration.create(1000, TimeUnit.MILLISECONDS);
        Future<Object> future = Patterns.ask(session, new GetPlayerRank(), timeout.length());
        PlayerRankMsg result = null;
        try{
            result = (PlayerRankMsg) Await.result(future, timeout);
        }
        catch (Exception e){
            LOG.catching(e);
        }
        return Option.some(Integer.valueOf(result.getRank()));
    }

    @Override
    public void startRound() {
        session.tell(new StartRound(), getSelf());
    }

    @Override
    public void signOut() {
        session.tell(PoisonPill.getInstance(), getSelf());
    }

    @Override
    public void setGamelogicEventsHandler(GamelogicEvents handler){
        glehandler = handler;
    }

    private ActorRef getSelf() {
        return TypedActor.context().self();
    }
}
