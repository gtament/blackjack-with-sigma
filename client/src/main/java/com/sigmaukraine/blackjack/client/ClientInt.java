package com.sigmaukraine.blackjack.client;

import akka.japi.Option;
import com.sigmaukraine.blackjack.common.msgs.GameroomInfo;
import com.sigmaukraine.blackjack.common.msgs.PlayerInfo;

import java.util.List;

public interface ClientInt {
    public Option<PlayerInfo> signIn(String username, String password);
    public Option<PlayerInfo> signUp(String username, String password);
    public Option<Boolean> establishConnection();
    public Option<List<PlayerInfo>> getLeaderboard(int from, int count);
    public Option<List<GameroomInfo>> getGamerooms();
    public Option<List<String>> joinGameroom(GameroomInfo info);
    public Option<Boolean> createGameroom(GameroomInfo info);
    public void setGamelogicEventsHandler(GamelogicEvents handler);
    public Option<Integer> getPlayerRank();
    public void startRound();
    public void signOut();
}
