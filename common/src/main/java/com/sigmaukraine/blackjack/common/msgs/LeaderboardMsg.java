package com.sigmaukraine.blackjack.common.msgs;

import java.util.ArrayList;
import java.util.List;

public class LeaderboardMsg extends Msg{

    List <PlayerInfo> board;

    public LeaderboardMsg(){
        board = new ArrayList<PlayerInfo>();
    }

    public void add(String username, Long cash, Long wins, Long loses){
        board.add(new PlayerInfo(username, cash, wins, loses));
    }

    public List<PlayerInfo> getBoard() {
        return board;
    }

    public void add(PlayerInfo playerInfo) {
        board.add(playerInfo);
    }
}
