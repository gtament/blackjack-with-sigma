package com.sigmaukraine.blackjack.common.msgs;

/**
 * Created by gtament on 04.10.2014.
 */
public class AuthInfo extends Msg {

    private boolean signUp;
    private String username;
    private String password;

    public AuthInfo(String username, String password){
        this.username=username;
        this.password=password;
        signUp=false;
    }

    public AuthInfo(String username, String password, boolean signUp){
        this.username=username;
        this.password=password;
        this.signUp=signUp;
    }

    @Override
    public String toString(){
        StringBuilder res = new StringBuilder();
        res.append(username);
        res.append(" ");
        res.append(password);
        return res.toString();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSignUp() {
        return signUp;
    }
}
