package com.sigmaukraine.blackjack.common.msgs;

/**
 * Created by gtament on 26.10.2014.
 */
public class PlayerInfo extends Msg{
    private String username;
    private Long cash = 200L;
    private Long wins = 0L;
    private Long loses = 0L;

    public PlayerInfo(String username, Long cash, Long wins, Long loses) {
        this.username = username;
        this.cash = cash;
        this.wins = wins;
        this.loses = loses;
    }

    public String getUsername() {
        return username;
    }

    public Long getCash() {
        return cash;
    }

    public Long getWins() {
        return wins;
    }

    public Long getLoses() {
        return loses;
    }

    public void setCash(Long cash) {
        this.cash = cash;
    }

    public void incWins() {
        wins++;
    }

    public void incLoses() {
        loses++;
    }
}
