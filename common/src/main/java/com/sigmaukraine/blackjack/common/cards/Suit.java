package com.sigmaukraine.blackjack.common.cards;

import java.io.Serializable;

public enum Suit implements Serializable{
    SPADES, DIAMONDS, CLUBS, HEARTS;
}
