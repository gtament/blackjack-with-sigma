package com.sigmaukraine.blackjack.common.cards;

import java.io.Serializable;

public class Card implements Serializable{
    private Suit suit;
    private Dignity dignity;

    public Card(Suit suit, Dignity dignity) {
        this.suit = suit;
        this.dignity = dignity;
    }

    public Suit getSuit() {
        return suit;
    }

    public Dignity getDignity() {
        return dignity;
    }

    @Override
    public String toString() {
        return dignity + " of " +
                suit;
    }

    @Override
    public boolean equals(Object obj) {
        Card c;
        if(obj instanceof Card) {
            c = (Card) obj;
            if(suit == c.suit && dignity == c.dignity) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return suit.ordinal()*100 + dignity.hashCode();
    }
}
