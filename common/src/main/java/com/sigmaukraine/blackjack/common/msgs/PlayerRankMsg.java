package com.sigmaukraine.blackjack.common.msgs;

public class PlayerRankMsg extends Msg{
    private int rank;

    public PlayerRankMsg(int rank) {
        this.rank = rank;
    }

    public int getRank() {
        return rank;
    }
}
