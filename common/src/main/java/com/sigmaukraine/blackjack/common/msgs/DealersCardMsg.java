package com.sigmaukraine.blackjack.common.msgs;

import com.sigmaukraine.blackjack.common.cards.Card;

/**
 * Created by gtament on 23.11.2014.
 */
public class DealersCardMsg extends Msg{
    private Card card;
    private boolean show;

    public DealersCardMsg(Card card, boolean show) {
        this.card = card;
        this.show = show;
    }

    public Card getCard() {
        return card;
    }

    public boolean isShowed() {
        return show;
    }
}
