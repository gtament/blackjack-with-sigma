package com.sigmaukraine.blackjack.common.msgs;

import java.util.List;

public class JoinSuccess extends JoinResult {
    List<String> players;

    public JoinSuccess(List<String> players) {
        this.players = players;
    }

    public List<String> getPlayers() {
        return players;
    }
}
