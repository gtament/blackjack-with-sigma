package com.sigmaukraine.blackjack.common.msgs;

public class GetLeaderboard extends Msg{

    int from, count;

    public GetLeaderboard(int from, int count) {
        this.from = from;
        this.count = count;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
