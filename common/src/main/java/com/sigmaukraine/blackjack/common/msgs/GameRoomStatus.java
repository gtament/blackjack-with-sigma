package com.sigmaukraine.blackjack.common.msgs;

import java.io.Serializable;

public enum GameRoomStatus  implements Serializable {
    CAN_CONNECT, RUNNING_ROUND, IS_FULL;
}
