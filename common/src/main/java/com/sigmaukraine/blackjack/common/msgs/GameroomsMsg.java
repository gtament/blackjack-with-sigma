package com.sigmaukraine.blackjack.common.msgs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gtament on 23.11.2014.
 */
public class GameroomsMsg extends Msg {
    private List<GameroomInfo> rooms;

    public GameroomsMsg(){
        rooms = new ArrayList<GameroomInfo>();
    }

    public void add(String name, long minBet, int maxPlayers){
        rooms.add(new GameroomInfo(name, minBet, maxPlayers));
    }

    public void add(GameroomInfo info) {
        rooms.add(info);
    }

    public List<GameroomInfo> getRooms() {
        return rooms;
    }
}
