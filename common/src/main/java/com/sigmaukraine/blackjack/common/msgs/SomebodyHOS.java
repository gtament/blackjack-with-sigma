package com.sigmaukraine.blackjack.common.msgs;

/**
 * Created by gtament on 23.11.2014.
 */
public class SomebodyHOS extends Msg{

    private String name;
    private boolean hit;

    public SomebodyHOS(String name, boolean hit) {
        this.name = name;
        this.hit = hit;
    }

    public String getName() {
        return name;
    }

    public boolean getHOS() {
        return hit;
    }
}
