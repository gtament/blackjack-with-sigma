package com.sigmaukraine.blackjack.common.msgs;

public class CreateGameroomMsg extends GameroomInfo {

    public CreateGameroomMsg(String name, long minBet, int maxPlayers) {
        super(name, minBet, maxPlayers);
    }

    public CreateGameroomMsg(GameroomInfo info) {
        super(info);
    }

    public GameroomInfo getInfo(){
        return this;
    }
}
