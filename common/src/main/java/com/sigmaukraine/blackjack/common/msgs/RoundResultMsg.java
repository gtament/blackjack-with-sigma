package com.sigmaukraine.blackjack.common.msgs;

public class RoundResultMsg extends Msg{
    private String name;
    private long winning;

    public RoundResultMsg(String name, long winning) {
        this.name = name;
        this.winning = winning;
    }

    public String getName() {
        return name;
    }

    public long getWinning() {
        return winning;
    }
}
