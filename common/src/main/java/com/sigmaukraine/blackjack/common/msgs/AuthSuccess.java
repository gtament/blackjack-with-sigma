package com.sigmaukraine.blackjack.common.msgs;

public class AuthSuccess extends AuthResult {
    private PlayerInfo info;

    public AuthSuccess(PlayerInfo info) {
        this.info = info;
    }

    public PlayerInfo getInfo() {
        return info;
    }
}
