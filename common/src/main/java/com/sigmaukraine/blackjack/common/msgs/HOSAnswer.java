package com.sigmaukraine.blackjack.common.msgs;

/**
 * Created by gtament on 23.11.2014.
 */
public class HOSAnswer extends Msg{

    private Boolean hit;

    public HOSAnswer(Boolean hit) {
        this.hit = hit;
    }

    public Boolean getHOS() {
        return hit;
    }
}
