package com.sigmaukraine.blackjack.common.msgs;

public class GameroomInfo extends Msg {
    private GameRoomStatus status;
    private String name;
    private long minBet;
    private int curPlayers;
    private int maxPlayers;

    public GameroomInfo(String name, long minBet, int maxPlayers, int curPlayers) {
        this.name = name;
        this.minBet = minBet;
        this.maxPlayers = maxPlayers;
        this.curPlayers = curPlayers;
    }

    public GameroomInfo(String name, long minBet, int maxPlayers, int curPlayers, GameRoomStatus status) {
        this.name = name;
        this.minBet = minBet;
        this.maxPlayers = maxPlayers;
        this.curPlayers = curPlayers;
        this.status = status;
    }

    public GameroomInfo(String name, long minBet, int maxPlayers) {
        this.name = name;
        this.minBet = minBet;
        this.maxPlayers = maxPlayers;
        this.curPlayers = 1;
    }

    public GameroomInfo(GameroomInfo info){
        this.name = info.name;
        this.minBet = info.minBet;
        this.curPlayers = info.curPlayers;
        this.maxPlayers =  info.maxPlayers;
    }

    public String getName() {
        return name;
    }

    public long getMinBet() {
        return minBet;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public int getCurPlayers() {
        return curPlayers;
    }

    public GameRoomStatus getStatus() {
        return status;
    }
}
