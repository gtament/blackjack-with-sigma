package com.sigmaukraine.blackjack.common.msgs;

/**
 * Created by gtament on 23.11.2014.
 */
public class SomebodyBet extends Msg{

    private String name;
    private long bet;

    public SomebodyBet(String name, long bet) {
        this.name = name;
        this.bet = bet;
    }

    public String getName() {
        return name;
    }

    public long getBet() {
        return bet;
    }
}
