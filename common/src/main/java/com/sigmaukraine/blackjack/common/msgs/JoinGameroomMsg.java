package com.sigmaukraine.blackjack.common.msgs;

public class JoinGameroomMsg extends GameroomInfo {

    public JoinGameroomMsg(String name, long minBet, int maxPlayers) {
        super(name, minBet, maxPlayers);
    }

    public JoinGameroomMsg(GameroomInfo info) {
        super(info);
    }

    public GameroomInfo getInfo(){
        return this;
    }

}
