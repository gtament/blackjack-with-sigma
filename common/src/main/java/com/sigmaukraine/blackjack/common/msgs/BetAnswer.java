package com.sigmaukraine.blackjack.common.msgs;

/**
 * Created by gtament on 22.11.2014.
 */
public class BetAnswer extends Msg {

    private Long bet;

    public BetAnswer(Long bet) {
        this.bet = bet;
    }

    public Long getBet() {
        return bet;
    }
}
