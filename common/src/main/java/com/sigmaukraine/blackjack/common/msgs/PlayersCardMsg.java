package com.sigmaukraine.blackjack.common.msgs;

import com.sigmaukraine.blackjack.common.cards.Card;

/**
 * Created by gtament on 23.11.2014.
 */
public class PlayersCardMsg extends Msg{
    private String name;
    private Card card;

    public PlayersCardMsg(String name, Card card) {
        this.name = name;
        this.card = card;
    }

    public String getName() {
        return name;
    }

    public Card getCard() {
        return card;
    }
}
