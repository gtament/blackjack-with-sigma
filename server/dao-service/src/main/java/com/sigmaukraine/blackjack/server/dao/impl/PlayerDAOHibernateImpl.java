package com.sigmaukraine.blackjack.server.dao.impl;

import com.sigmaukraine.blackjack.server.dao.entity.Player;
import com.sigmaukraine.blackjack.server.dao.service.PlayerDAOService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import java.util.List;

public class PlayerDAOHibernateImpl implements PlayerDAOService {

    private SessionFactory sessionFactory;

    private static final Logger LOG = LogManager.getLogger(PlayerDAOHibernateImpl.class.getName());

    public PlayerDAOHibernateImpl(BundleContext context) {
        ServiceReference sr = context.getServiceReference(SessionFactory.class.getName());
        sessionFactory = (SessionFactory) context.getService(sr);
    }

    @Override
    public void savePlayer(Player player) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.save(player);
            session.getTransaction().commit();
        }
        catch (HibernateException e) {
            session.getTransaction().rollback();
            LOG.catching(e);
        }
        session.close();
    }

    @Override
    public void updatePlayer(Player player) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(player);
            session.getTransaction().commit();
        }
        catch (HibernateException e) {
            session.getTransaction().rollback();
            LOG.catching(e);
        }
        session.close();
    }

    @Override
    public Player getPlayer(String username) {
        Player player = null;
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            List<Player> result = session.createQuery("from Player p where p.username = :username")
                                         .setString("username", username)
                                         .list();
            if(!result.isEmpty()) {
                player = result.get(0);
            }
            session.getTransaction().commit();
        }
        catch (HibernateException e) {
            session.getTransaction().rollback();
            LOG.catching(e);
        }
        session.close();
        return player;
    }

    @Override
    public Player getPlayer(String username, String password) {
        Player player = null;
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            player = (Player)session.byNaturalId(Player.class).using("username", username)
                                                              .using("password", password).load();
            session.getTransaction().commit();
        }
        catch (HibernateException e) {
            session.getTransaction().rollback();
            LOG.catching(e);
        }
        session.close();
        return player;
    }

    @Override
    public List<Player> getPlayersRankedList(int from, int count) {
        List<Player> players = null;
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            players = session.createQuery("from Player p order by p.cash desc")
                              .setFirstResult(from)
                              .setMaxResults(count)
                              .list();
            session.getTransaction().commit();
        }
        catch (HibernateException e) {
            session.getTransaction().rollback();
            LOG.catching(e);
        }
        session.close();
        return players;
    }

    @Override
    public int getPlayerRank(Player player) {
        List<Player> players;
        int rank = 0;
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            players = session.createQuery("from Player p order by p.cash desc").list();
            if(!players.isEmpty()) {
                rank = players.indexOf(player);
            }
            session.getTransaction().commit();
        }
        catch (HibernateException e) {
            session.getTransaction().rollback();
            LOG.catching(e);
        }
        session.close();
        return rank;
    }
}
