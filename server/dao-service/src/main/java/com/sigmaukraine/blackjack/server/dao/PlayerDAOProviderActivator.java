package com.sigmaukraine.blackjack.server.dao;

import com.sigmaukraine.blackjack.server.dao.impl.PlayerDAOHibernateImpl;
import com.sigmaukraine.blackjack.server.dao.service.PlayerDAOService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class PlayerDAOProviderActivator implements BundleActivator {
    private ServiceRegistration serviceRegistration;

    private static final Logger LOG = LogManager.getLogger(PlayerDAOProviderActivator.class.getName());

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        serviceRegistration = bundleContext.registerService(PlayerDAOService.class.getName(), new PlayerDAOHibernateImpl(bundleContext), null);
        LOG.info("PlayerDAOHibernateImpl service registered.");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        serviceRegistration.unregister();
        LOG.info("PlayerDAOHibernateImpl service unregistered.");
    }
}
