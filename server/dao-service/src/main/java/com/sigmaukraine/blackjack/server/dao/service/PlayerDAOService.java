package com.sigmaukraine.blackjack.server.dao.service;

import com.sigmaukraine.blackjack.server.dao.entity.Player;

import java.io.Serializable;
import java.util.List;

public interface PlayerDAOService extends Serializable {

    void savePlayer(Player player);

    void updatePlayer(Player player);

    Player getPlayer(String username);

    Player getPlayer(String username, String password);

    List<Player> getPlayersRankedList(int from, int count);

    int getPlayerRank(Player player);
}
