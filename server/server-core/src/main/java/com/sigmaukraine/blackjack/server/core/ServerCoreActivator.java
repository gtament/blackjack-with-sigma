package com.sigmaukraine.blackjack.server.core;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.osgi.ActorSystemActivator;
import com.sigmaukraine.blackjack.server.dao.service.PlayerDAOService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class ServerCoreActivator extends ActorSystemActivator {

    private ServiceReference pdaoReference;
    private GameroomStorage gameroomStorage;

    private static final Logger LOG = LogManager.getLogger(ServerCoreActivator.class.getName());

    @Override
    public void configure(BundleContext bundleContext, ActorSystem actorSystem) {
        pdaoReference = bundleContext.getServiceReference(PlayerDAOService.class.getName());
        gameroomStorage = ListGameroomStorage.getInstance();
        PlayerDAOService pdao = serviceForReference(bundleContext, pdaoReference);
        LOG.info("PlayerDAO service acquired.");

        actorSystem.actorOf(Props.create(ConnectionAccepter.class, pdao, gameroomStorage), "connaccept");
    }

    @Override
    public void start(BundleContext context) {
        super.start(context);

        LOG.info("Server started.");
    }

    @Override
    public void stop(BundleContext context) {
        super.stop(context);

        LOG.info("Server stopped.");
    }

    @Override
    public String getActorSystemName(BundleContext context) {
        return "BlackjackServer";
    }
}
