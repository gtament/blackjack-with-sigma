package com.sigmaukraine.blackjack.server.core.gamelogic;

import akka.japi.Option;
import com.sigmaukraine.blackjack.common.cards.Card;
import com.sigmaukraine.blackjack.common.msgs.GameRoomStatus;
import com.sigmaukraine.blackjack.common.msgs.GameroomInfo;
import com.sigmaukraine.blackjack.server.core.SessionInt;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class GameRoom {

    private List<LogicPlayer> players;
    private GameRoomStatus gameRoomStatus = GameRoomStatus.CAN_CONNECT;
    private int maxPlayerNumber;
    private long minBet;
    private String name;

    public GameRoom(SessionInt player, String name, int maxPlayersNumber, long minBet) {
        this.name = name;
        this.minBet = minBet;
        this.maxPlayerNumber = maxPlayersNumber;
        players = new ArrayList<>(maxPlayersNumber);
        join(player);
    }

    public static GameRoom createFromInfo(SessionInt player, GameroomInfo info){
        return new GameRoom(player, info.getName(), info.getMaxPlayers(), info.getMinBet());
    }

    long requestBet(LogicPlayer player) {
        long bet;
        Option<Long> answer = player.getMailbox().requestBet();
        bet = answer.get().longValue();
        for(LogicPlayer p : players){
            if(player==p)
                continue;
            p.getMailbox().sendRB(bet, player.getName());
        }
        return bet;
    }

    boolean requestHitOrStand(LogicPlayer player) {
        boolean hit;
        Option<Boolean> answer = player.getMailbox().requestHitOrStand();
        hit = answer.get().booleanValue();
        for(LogicPlayer p : players){
            if(player==p)
                continue;
            p.getMailbox().sendRHOS(hit, player.getName());
        }
        return hit;
    }

    void sendPlayersCard(Card card, LogicPlayer player) {
        for(LogicPlayer p : players)
            p.getMailbox().sendPlayersCard(card, player.getName());
    }

    void sendDealersCard(Card card, boolean show) {
        for(LogicPlayer p : players)
            p.getMailbox().sendDealersCard(card, show);

    }

    void sendResult(LogicPlayer player, long winning) {
        for(LogicPlayer p : players)
            p.getMailbox().sendResult(winning, player.getName());
    }

    void sendOpeningDealersCard() {
        for(LogicPlayer p : players)
            p.getMailbox().sendOpeningDealersCards();
    }

    public GameRoomStatus getGameRoomStatus() {
        return gameRoomStatus;
    }

    public int getMaxPlayerNumber() {
        return maxPlayerNumber;
    }

    public List<String> join(SessionInt player) {
        if(players.size()+1>maxPlayerNumber){
            gameRoomStatus = GameRoomStatus.IS_FULL;
            return new ArrayList<>();
        }
        boolean tempS;
        if (gameRoomStatus==GameRoomStatus.RUNNING_ROUND)
            tempS=true;
        else
            tempS=false;
        LogicPlayer internalP = new LogicPlayer(player, tempS);
        players.add(internalP);
        List<String> playersNames = new ArrayList<>(maxPlayerNumber);
        for(LogicPlayer p : players)
            playersNames.add(p.getName());
        return playersNames;
    }

    public void playRound() {
        gameRoomStatus = GameRoomStatus.RUNNING_ROUND;

        for(LogicPlayer p : players)
            requestBet(p);

        Dealer dealer = new Dealer(this);

        /* Give hidden card to dealer */
        dealer.giveCardToDealer(false);

        /* Give 2 cards to player */
        for (LogicPlayer player : players) {
            if(!player.isSpectating()){
                dealer.giveCardToPlayer(player);
                dealer.giveCardToPlayer(player);
            }
        }

        /* Play with players*/
        for (LogicPlayer player : players) {
            if(!player.isSpectating()) {
                dealer.playWithPlayer(player);
            }
        } 

        /* Dealers game */
        dealer.playHisGame();

        /* Make result */
        for (LogicPlayer player : players) {
            if(!player.isSpectating())
                sendResult(player, dealer.makeResult(player));
        }

    }

    public String getName() {
        return name;
    }

    public GameroomInfo generateInfo() {
        return new GameroomInfo(name, minBet, maxPlayerNumber, players.size(), gameRoomStatus);
    }

    public void executeRound(){
        LogicPlayer p = players.get(0);
        if(p==null)
            return;
        p.getMailbox().executeSome(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                playRound();
                return null;
            }
        });
    }
}
