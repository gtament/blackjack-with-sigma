package com.sigmaukraine.blackjack.server.core.gamelogic;

import com.sigmaukraine.blackjack.common.cards.Card;

public class Dealer {

    public static final int MAX_PLAYERS_POINTS = 21;
    public static final int MAX_DEALERS_POINTS = 17;
    public static final double PRIZE_COEFFICIENT = 1.5;

    private int dealersPoints = 0;
    private boolean dealerExceeded = false;
    private Deck deck = new Deck();
    private GameRoom room;

    public Dealer(GameRoom room) {
        this.room = room;
        deck.initDeck();
        deck.shuffle();
    }

    void giveCardToPlayer(LogicPlayer player) {
        Card card = deck.getNextCard();
        player.increasePoints(card.getDignity().getPoints());
        room.sendPlayersCard(card, player);
    }

    void giveCardToDealer(boolean show) {
        Card card = deck.getNextCard();
        dealersPoints += card.getDignity().getPoints();
        dealerExceeded = dealersPoints >= MAX_DEALERS_POINTS;
        room.sendDealersCard(card, show);
    }

    void playWithPlayer(LogicPlayer player) {
        while (room.requestHitOrStand(player) && player.getPoints() < MAX_PLAYERS_POINTS) {
            giveCardToPlayer(player);
        }
    }

    void playHisGame() {
        room.sendOpeningDealersCard();
        while (!dealerExceeded) {
            giveCardToDealer(true);
        }
    }

    long makeResult(LogicPlayer player) {
        long winning;
        if (player.getPoints() > dealersPoints && player.getPoints() <= MAX_PLAYERS_POINTS) {
            winning = (long) (player.getBet() * PRIZE_COEFFICIENT);
        } else {
            winning = -player.getBet();
        }
        player.setCash(player.getCash() + winning);
        return winning;
    }
}
