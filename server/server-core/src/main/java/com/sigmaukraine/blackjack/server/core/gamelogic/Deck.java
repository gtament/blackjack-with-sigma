package com.sigmaukraine.blackjack.server.core.gamelogic;

import com.sigmaukraine.blackjack.common.cards.Card;
import com.sigmaukraine.blackjack.common.cards.Dignity;
import com.sigmaukraine.blackjack.common.cards.Suit;

public class Deck {

    public static final int DECKS_NUM = 2;
    public static final int CARDS_NUM = 52;
    private Card[] cards = new Card[DECKS_NUM * CARDS_NUM];
    private int cardOnDeckIndex = 0;

    Card getNextCard() {
        Card retVal = cards[cardOnDeckIndex];
        cardOnDeckIndex++;
        return retVal;
    }

    void initDeck() {
        Dignity[] dignities = Dignity.values();
        Suit[] suits = Suit.values();

        for (int i = 0; i < DECKS_NUM; i++) {
            for (int j = 0; j < CARDS_NUM; j++) {
                int cardIndex = i * CARDS_NUM + j;
                cards[cardIndex] = new Card(suits[cardIndex % suits.length],
                        dignities[cardIndex % dignities.length]);
            }
        }
    }

    void shuffle() {
        for (int i = 0; i < DECKS_NUM * CARDS_NUM; i++) {
            int k;

            do {
                k = (int) (Math.random() * CARDS_NUM);
            } while (k == i);

            Card tmp = cards[k];
            cards[k] = cards[i];
            cards[i] = tmp;
        }
    }
}
