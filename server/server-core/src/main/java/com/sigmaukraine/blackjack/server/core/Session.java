package com.sigmaukraine.blackjack.server.core;

import akka.actor.*;
import akka.japi.Option;
import akka.pattern.Patterns;
import com.sigmaukraine.blackjack.common.cards.Card;
import com.sigmaukraine.blackjack.common.msgs.*;
import com.sigmaukraine.blackjack.server.core.gamelogic.GameRoom;
import com.sigmaukraine.blackjack.server.dao.entity.Player;
import com.sigmaukraine.blackjack.server.dao.service.PlayerDAOService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import scala.concurrent.Await;
import scala.concurrent.ExecutionContextExecutor;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import scala.runtime.AbstractPartialFunction;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static akka.dispatch.Futures.future;

public class Session implements SessionInt, TypedActor.Receiver {
    private final PlayerDAOService pdao;
    private final GameroomStorage gameroomStorage;
    private Player myPlayer;
    private ActorRef returnAddress;
    private GameRoom room;

    private static final transient Logger LOG = LogManager.getLogger(Session.class.getName());

    public Session(PlayerDAOService pdao, GameroomStorage gameroomStorage){
        this.pdao = pdao;
        this.gameroomStorage = gameroomStorage;
    }

    @Override
    public void onReceive(Object msg, ActorRef sender){
        returnAddress = sender;
        LOG.info("Session: " + msg.toString());
        if(msg instanceof AuthInfo){

            if(!((AuthInfo) msg).isSignUp()){
                Player player = pdao.getPlayer(((AuthInfo) msg).getUsername(), ((AuthInfo) msg).getPassword());
                if(player == null){
                    sender.tell(new AuthFail(), getSelf());
                }
                else{
                    myPlayer=player;
                    sender.tell(new AuthSuccess(new PlayerInfo(myPlayer.getUsername(), myPlayer.getCash(), myPlayer.getWins(), myPlayer.getLoses())), getSelf());
                    LOG.info(player.toString());
                }
            }
            else{
                Player player = pdao.getPlayer(((AuthInfo) msg).getUsername());
                if(player==null){
                    Player regPlayer = new Player(((AuthInfo) msg).getUsername(), ((AuthInfo) msg).getPassword());
                    pdao.savePlayer(regPlayer);
                    myPlayer=regPlayer;
                    sender.tell(new AuthSuccess(new PlayerInfo(myPlayer.getUsername(), myPlayer.getCash(), myPlayer.getWins(), myPlayer.getLoses())), getSelf());
                }
                else{
                    sender.tell(new AuthFail(), getSelf());
                }
            }
            return;
        }
        if(msg instanceof GetLeaderboard){
            sender.tell(generateLeaderboard(pdao.getPlayersRankedList(((GetLeaderboard) msg).getFrom(), ((GetLeaderboard) msg).getCount())), getSelf());
            return;
        }
        if(msg instanceof GetGamerooms){
            sender.tell(generateGameroomsList(gameroomStorage.getAllGameRooms()), getSelf());
            return;
        }
        if(msg instanceof CreateGameroomMsg){
            if(gameroomStorage.getGameRoom(((CreateGameroomMsg) msg).getName())!=null){
                sender.tell(new NonUniqueGameroomName(), getSelf());
                return;
            }
            GameRoom temp = GameRoom.createFromInfo(this, ((CreateGameroomMsg) msg).getInfo());
            gameroomStorage.saveGameRoom(temp);
            room=temp;
            sender.tell(new CreateSuccess(), getSelf());
            return;
        }
        if(msg instanceof JoinGameroomMsg){
            GameRoom temp;
            if((temp= gameroomStorage.getGameRoom(((JoinGameroomMsg) msg).getName()))==null){
                sender.tell(new NonExistingGameroomName(), getSelf());
                return;
            }
            sender.tell(new JoinSuccess(temp.join(this)), getSelf());
            return;
        }
        if(msg instanceof GetPlayerRank){
            sender.tell(new PlayerRankMsg(pdao.getPlayerRank(myPlayer)), getSelf());
            return;
        }
        if(msg instanceof StartRound){
            updateReturnAddress();
            if(room!=null)
                room.playRound();
            return;
        }
        sender.tell(new UnsupportedMsg(), getSelf());
    }

    private LeaderboardMsg generateLeaderboard(List<Player> playersRankedList) {
        LeaderboardMsg board = new LeaderboardMsg();
        for(Player p : playersRankedList){
            board.add(p.getUsername(), p.getCash(), p.getWins(), p.getLoses());
        }
        return board;
    }

    private GameroomsMsg generateGameroomsList(List<GameRoom> gameRooms){
        GameroomsMsg msg = new GameroomsMsg();
        for(GameRoom g : gameRooms)
            msg.add(g.generateInfo());
        return msg;
    }

    @Override
    public Option<Long> requestBet() {
        Duration timeout = Duration.create(5, TimeUnit.MINUTES);
        Future<Object> future = Patterns.ask(returnAddress, new RequestBet(), timeout.length());
        BetAnswer bet = null;
        try{
            bet = (BetAnswer) Await.result(future, timeout);
        }
        catch(Exception e){
            LOG.catching(e);
        }
        if(bet!=null){
            return Option.some(bet.getBet());
        }
        return null;
    }

    @Override
    public Option<Boolean> requestHitOrStand() {
        Duration timeout = Duration.create(5, TimeUnit.MINUTES);
        Future<Object> future = Patterns.ask(returnAddress, new RequestHitOrStand(), timeout.length());
        HOSAnswer hit = null;
        try{
            hit = (HOSAnswer) Await.result(future, timeout);
        }
        catch (Exception e){
            LOG.catching(e);
        }
        if(hit!=null)
            return Option.some(hit.getHOS());
        return null;
    }

    @Override
    public void sendRB(long bet, String name) {
        returnAddress.tell(new SomebodyBet(name, bet), getSelf());
    }

    @Override
    public void sendRHOS(boolean hos, String name) {
        returnAddress.tell(new SomebodyHOS(name, hos), getSelf());
    }

    @Override
    public void sendDealersCard(Card card, boolean show) {
        returnAddress.tell(new DealersCardMsg(card, show), getSelf());
    }

    @Override
    public void sendPlayersCard(Card card, String name) {
        returnAddress.tell(new PlayersCardMsg(name, card), getSelf());
    }

    @Override
    public void sendResult(long winning, String name) {
        if(myPlayer.getUsername().equals(name)){
            myPlayer.setCash(myPlayer.getCash()+winning);
            if(winning<0)
                myPlayer.incLoses();
            else
                myPlayer.incWins();
        }
        returnAddress.tell(new RoundResultMsg(name, winning), getSelf());
    }

    @Override
    public void sendOpeningDealersCards() {
        returnAddress.tell(new OpenDealersCardsMsg(), getSelf());
    }

    @Override
    public Long getCash(){
        return myPlayer.getCash();
    }

    @Override
    public String getName() {
        return myPlayer.getUsername();
    }

    @Override
    public void executeSome(Callable<Object> c) {
        ExecutionContextExecutor dispatcher = TypedActor.context().dispatcher();
        Future<Object> f = future(c, dispatcher);
        f.onSuccess(new AbstractPartialFunction<Object, Object>() {
            @Override
            public boolean isDefinedAt(Object o) {
                return false;
            }
        }, dispatcher);
    }

    private ActorRef getSelf() {
        return TypedActor.context().self();
    }

    @Override
    public Option<Object> updateReturnAddress(){
        ActorSelection rABefore = TypedActor.context().actorSelection(returnAddress.path());
        Duration timeout = Duration.create(1000, TimeUnit.MILLISECONDS);
        Future<Object> connectToServer = Patterns.ask(rABefore, new Identify(returnAddress.path()), timeout.length());
        ActorIdentity temp = null;
        try {
            temp = (ActorIdentity) Await.result(connectToServer, timeout);
        }
        catch (Exception e) {
            LOG.catching(e);
        }
        returnAddress = temp.getRef();
        return Option.some(new Object());
    }


}
