package com.sigmaukraine.blackjack.server.core;

import akka.japi.Option;
import com.sigmaukraine.blackjack.common.cards.Card;

import java.util.concurrent.Callable;

public interface SessionInt {
    public Option<Long> requestBet();
    public Option<Boolean> requestHitOrStand();
    public void sendRB(long bet, String name);
    public void sendRHOS(boolean hos, String name);
    public void sendPlayersCard(Card card, String name);
    public void sendDealersCard(Card card, boolean isShowed);
    public void sendResult(long winning, String name);
    public void sendOpeningDealersCards();
    public Long getCash();
    public String getName();
    public void executeSome(Callable<Object> c);
    public Option<Object> updateReturnAddress();
}
