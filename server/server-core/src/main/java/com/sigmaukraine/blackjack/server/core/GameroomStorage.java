package com.sigmaukraine.blackjack.server.core;

import com.sigmaukraine.blackjack.server.core.gamelogic.GameRoom;

import java.util.List;

public interface GameroomStorage {
    public List<GameRoom> getAllGameRooms();
    public GameRoom getGameRoom(String name);
    public boolean saveGameRoom(GameRoom gameRoom);
}
