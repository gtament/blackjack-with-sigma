package com.sigmaukraine.blackjack.server.core;

import com.sigmaukraine.blackjack.server.core.gamelogic.GameRoom;

import java.util.LinkedList;
import java.util.List;

public class ListGameroomStorage implements GameroomStorage {

    private List<GameRoom> rooms;

    private static ListGameroomStorage ourInstance = new ListGameroomStorage();

    private ListGameroomStorage() {
        rooms=new LinkedList<>();
    }

    public static ListGameroomStorage getInstance() {
        return ourInstance;
    }

    @Override
    public synchronized List<GameRoom> getAllGameRooms(){
        return rooms;
    }

    @Override
    public synchronized GameRoom getGameRoom(String name){
        for(GameRoom g : rooms){
            if(g.getName().equals(name))
                return g;
        }
        return null;
    }

    @Override
    public synchronized boolean saveGameRoom(GameRoom gameRoom){
        rooms.add(gameRoom);
        return true;
    }

}
