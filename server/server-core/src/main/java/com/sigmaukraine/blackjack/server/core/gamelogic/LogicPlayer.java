package com.sigmaukraine.blackjack.server.core.gamelogic;

import com.sigmaukraine.blackjack.server.core.SessionInt;

public class LogicPlayer {
    private long bet = 0;
    private long cash;
    private int points = 0;
    private String name;
    private SessionInt mailbox;
    private boolean spectating;


    public LogicPlayer(SessionInt player, boolean spectating) {
        mailbox=player;
        cash=mailbox.getCash();
        name=mailbox.getName();
        this.spectating=spectating;
    }

    long getCash() {
        return cash;
    }

    int getPoints() {
        return points;
    }

    void increasePoints(int val) {
        points += val;
    }

    public boolean isSpectating() {
        return spectating;
    }

    public SessionInt getMailbox() {
        return mailbox;
    }

    public String getName() {
        return name;
    }

    public long getBet() {
        return bet;
    }

    public void setCash(long cash) {
        this.cash = cash;
    }
}
