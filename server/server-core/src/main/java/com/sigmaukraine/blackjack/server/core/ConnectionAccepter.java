package com.sigmaukraine.blackjack.server.core;

import akka.actor.ActorRef;
import akka.actor.TypedActor;
import akka.actor.TypedProps;
import akka.actor.UntypedActor;
import akka.japi.Creator;
import com.sigmaukraine.blackjack.common.msgs.NewConn;
import com.sigmaukraine.blackjack.server.dao.service.PlayerDAOService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ConnectionAccepter extends UntypedActor {

    private final PlayerDAOService pdao;
    private final GameroomStorage gameroomStorage;

    private static final Logger LOG = LogManager.getLogger(ConnectionAccepter.class.getName());

    public ConnectionAccepter(PlayerDAOService pdao, GameroomStorage gameroomStorage){
        this.pdao=pdao;
        this.gameroomStorage = gameroomStorage;
    }

    @Override
    public void onReceive(Object msg){
        LOG.info(msg.toString());
        if(msg instanceof NewConn){
            SessionInt session =  TypedActor.get(getContext().system())
                    .typedActorOf(new TypedProps<Session>(SessionInt.class,
                            new Creator<Session>() {
                                public Session create() {
                                    return new Session (pdao, gameroomStorage);
                                }
                            }
                    ));
            ActorRef sessionRef = TypedActor.get(getContext().system())
                    .getActorRefFor(session);
            getSender().tell(sessionRef, getSelf());
        }
    }
}

