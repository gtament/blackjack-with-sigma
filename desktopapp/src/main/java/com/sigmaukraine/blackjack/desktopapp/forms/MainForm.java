package com.sigmaukraine.blackjack.desktopapp.forms;

import com.sigmaukraine.blackjack.client.ClientServerManager;
import com.sigmaukraine.blackjack.common.msgs.GameroomInfo;
import com.sigmaukraine.blackjack.common.msgs.PlayerInfo;
import com.sigmaukraine.blackjack.desktopapp.datamodels.GameroomListDataModel;
import com.sigmaukraine.blackjack.desktopapp.datamodels.LeaderboardDataModel;
import com.sigmaukraine.blackjack.desktopapp.datamodels.PlayerRankDataModel;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class MainForm {
    private JFrame frame;
    private JPanel parentPanel;
    private JTabbedPane tabbedPane1;
    private JTable gameroomsTable;
    private JButton joinGameroomButton;
    private JButton createNewGameroomButton;
    private JTable leaderboardsTable;
    private JTable playerRankTable;
    private JButton signOutButton;
    private JLabel usernameLabel;
    private JLabel cashLabel;

    private PlayerInfo playerInfo;

    public MainForm(final PlayerInfo playerInfo) {
        this.playerInfo = playerInfo;

        usernameLabel.setText(playerInfo.getUsername());
        cashLabel.setText(playerInfo.getCash().toString());

        signOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientServerManager.getInstance().signOut();
                SignInForm signInForm = new SignInForm();
                signInForm.show();
                MainForm.this.close();
            }
        });

        createNewGameroomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CreateGameroomDialog createGameroomDialog = new CreateGameroomDialog(MainForm.this);
                createGameroomDialog.show();
            }
        });

        joinGameroomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GameroomInfo gameroom =  ((GameroomListDataModel) gameroomsTable.getModel()).getGameroomInfo(gameroomsTable.getSelectedRow());
                List<String> players = ClientServerManager.getInstance().joinGameroom(gameroom);
                if( players != null) {
                    MainForm.this.close();
                    GameroomForm gameroomForm = new GameroomForm(gameroom, playerInfo, players);
                    gameroomForm.show();
                    ClientServerManager.getInstance().setGamelogicEventsHandler(gameroomForm.new GamelogicEventsImpl());
                }
                else {
                    JOptionPane.showMessageDialog(parentPanel,
                            "Could not join gameroom.",
                            "Error!",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        tabbedPane1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if(tabbedPane1.getSelectedIndex() == 0) {
                    getGameroomList();
                }
                else if(tabbedPane1.getSelectedIndex() == 1) {
                    getLeaderboard();
                }
            }
        });

        frame = new JFrame("Sigma Blackjack");
        frame.setContentPane(parentPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();

        getGameroomList();
    }

    public void show() {
        frame.setVisible(true);
    }

    public void close() {
        frame.setVisible(false);
        frame.dispose();
    }

    private void getGameroomList() {
        List<GameroomInfo> gamerooms = ClientServerManager.getInstance().getGamerooms();
        gameroomsTable.setModel(new GameroomListDataModel(gamerooms));
    }

    private void getLeaderboard() {
        List<PlayerInfo> leaderboard = ClientServerManager.getInstance().getLeaderboard(0, 100);
        leaderboardsTable.setModel(new LeaderboardDataModel(leaderboard));
        int rank = ClientServerManager.getInstance().getPlayerRank();
        playerRankTable.setModel(new PlayerRankDataModel(playerInfo, rank));
    }

    public JFrame getFrame() {
        return frame;
    }

    public PlayerInfo getPlayerInfo() {
        return playerInfo;
    }
}
