package com.sigmaukraine.blackjack.desktopapp.forms;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HitOrStandDialog {
    private JDialog dialog;
    private JPanel parentPanel;
    private JButton hitButton;
    private JButton standButton;

    boolean result;

    public HitOrStandDialog(final JFrame owner) {
        hitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                result = true;
                HitOrStandDialog.this.close();
            }
        });

        standButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                result = false;
                HitOrStandDialog.this.close();
            }
        });

        dialog = new JDialog(owner, "Hit or stand?", true);
        dialog.setContentPane(parentPanel);
        dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        dialog.pack();
    }

    public boolean show() {
        dialog.setVisible(true);
        return result;
    }

    private void close() {
        dialog.setVisible(false);
        dialog.dispose();
    }
}
