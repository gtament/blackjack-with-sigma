package com.sigmaukraine.blackjack.desktopapp.forms;

import com.sigmaukraine.blackjack.client.ClientServerManager;
import com.sigmaukraine.blackjack.common.msgs.PlayerInfo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class SignInForm {
    private JFrame frame;
    private JPanel parentPanel;
    private JTextField usernameTextField;
    private JTextField passwordTextField;
    private JButton signInButton;
    private JButton signUpButton;

    private ClientServerManager clientServerManager;

    private class SignInRunnable implements Runnable {

        private static final String ERROR_MSG = "Error!";
        private static final String NO_CONN_MSG = "Cannot connect to server!\n" +
                                                  "Please check your internet connection.";
        private static final String INVALID_USR_MSG = "Invalid username or password.\n" +
                                                       "Have no account yet? Sign up!";


        @Override
        public void run() {
            frame.getGlassPane().setCursor(new Cursor(Cursor.WAIT_CURSOR));
            frame.getGlassPane().setVisible(true);

            if (!clientServerManager.isConnected() && !clientServerManager.establishConnection()) {
                JOptionPane.showMessageDialog(parentPanel, NO_CONN_MSG, ERROR_MSG, JOptionPane.ERROR_MESSAGE);
                frame.getGlassPane().setCursor(Cursor.getDefaultCursor());
                frame.getGlassPane().setVisible(false);
                return;
            }
            PlayerInfo playerInfo = clientServerManager.signIn(usernameTextField.getText(), passwordTextField.getText());
            if (playerInfo != null) {
                MainForm mainForm = new MainForm(playerInfo);
                mainForm.show();
                SignInForm.this.close();
            }
            else {
                JOptionPane.showMessageDialog(parentPanel, INVALID_USR_MSG, ERROR_MSG, JOptionPane.ERROR_MESSAGE);
            }

            frame.getGlassPane().setCursor(Cursor.getDefaultCursor());
            frame.getGlassPane().setVisible(false);
        }
    }

    private class SignUpRunnable implements Runnable {

        private static final String ERROR_MSG = "Error!";
        private static final String NO_CONN_MSG = "Cannot connect to server!\n" +
                                                  "Please check your internet connection.";
        private static final String SUCCESS_MSG = "You have successfully signed up!\n" +
                                                  "Now sign in.";
        private static final String FAIL_MSG = "This username is already in use.\n" +
                                               "Try another one.";


        @Override
        public void run() {
            parentPanel.setCursor(new Cursor(Cursor.WAIT_CURSOR));
            parentPanel.setEnabled(false);

            if (!clientServerManager.isConnected() && !clientServerManager.establishConnection()) {
                JOptionPane.showMessageDialog(parentPanel, NO_CONN_MSG, ERROR_MSG, JOptionPane.ERROR_MESSAGE);
                parentPanel.setCursor(Cursor.getDefaultCursor());
                parentPanel.setEnabled(true);
                return;
            }
            PlayerInfo playerInfo = clientServerManager.signUp(usernameTextField.getText(), passwordTextField.getText());
            if (playerInfo != null) {
                JOptionPane.showMessageDialog(parentPanel, SUCCESS_MSG, "Success!", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(parentPanel, FAIL_MSG, ERROR_MSG, JOptionPane.ERROR_MESSAGE);
            }

            parentPanel.setCursor(Cursor.getDefaultCursor());
            parentPanel.setEnabled(true);
        }
    }

    public SignInForm() {
        clientServerManager = ClientServerManager.getInstance();

        signInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread thread = new Thread(new SignInRunnable());
                thread.start();
            }
        });

        signUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread thread = new Thread(new SignUpRunnable());
                thread.start();
            }
        });

        frame = new JFrame("Sign In");
        frame.setContentPane(parentPanel);
        frame.getGlassPane().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(frame.getGlassPane().isVisible()) {
                    e.consume();
                }
            }
        });
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
    }

    public void show() {
        frame.setVisible(true);
    }

    public void close() {
        frame.setVisible(false);
    }

    public static void main(String[] args) {
        SignInForm signInForm = new SignInForm();
        signInForm.show();
    }
}
