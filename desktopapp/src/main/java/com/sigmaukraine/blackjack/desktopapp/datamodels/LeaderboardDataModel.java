package com.sigmaukraine.blackjack.desktopapp.datamodels;

import com.sigmaukraine.blackjack.common.msgs.PlayerInfo;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class LeaderboardDataModel extends AbstractTableModel {

    private List<PlayerInfo> leaderboard;
    private static final String[] COL_NAMES = {"#", "Username", "Cash", "Wins", "Loses"};

    public LeaderboardDataModel(List<PlayerInfo> leaderboard) {
        this.leaderboard = leaderboard;
    }

    @Override
    public int getRowCount() {
        return leaderboard.size();
    }

    @Override
    public int getColumnCount() {
        return COL_NAMES.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        PlayerInfo info = leaderboard.get(rowIndex);
        switch (columnIndex) {
            case 0: return rowIndex+1;
            case 1: return info.getUsername();
            case 2: return info.getCash();
            case 3: return info.getWins();
            case 4: return info.getLoses();
            default: return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        return COL_NAMES[column];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
