package com.sigmaukraine.blackjack.desktopapp.forms;

import com.sigmaukraine.blackjack.client.GamelogicEvents;
import com.sigmaukraine.blackjack.common.cards.Card;
import com.sigmaukraine.blackjack.common.msgs.GameroomInfo;
import com.sigmaukraine.blackjack.common.msgs.PlayerInfo;
import com.sigmaukraine.blackjack.desktopapp.GameTableCanvas;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

public class GameroomForm {
    private JFrame frame;
    private JPanel parentPanel;
    private GameTableCanvas gameTableCanvas;
    private JTextArea logTextArea;
    private GameroomInfo gameroom;

    private PlayerInfo playerInfo;

    public class GamelogicEventsImpl implements GamelogicEvents {
        @Override
        public long onRequestBet() {
            long bet = new BetDialog(GameroomForm.this.frame, GameroomForm.this.gameroom, GameroomForm.this.playerInfo).show();
            playerInfo.setCash(playerInfo.getCash() - bet);
            GameroomForm.this.logTextArea.append("You bet $" + bet + "\n");
            return bet;
        }

        @Override
        public boolean onRequestHitOrStand() {
            return new HitOrStandDialog(GameroomForm.this.frame).show();
        }

        @Override
        public void onDealersCard(Card card, boolean isShowed) {
            GameroomForm.this.gameTableCanvas.giveDealerCard(card, isShowed);
        }

        @Override
        public void onPlayersCard(Card card, String playerName) {
            GameroomForm.this.gameTableCanvas.givePlayerCard(card, true, playerName);
        }

        @Override
        public void onOpenDealersCard() {
            GameroomForm.this.gameTableCanvas.openDealerCards();
        }

        @Override
        public void onSomebodyBet(long bet, String name) {
            GameroomForm.this.gameTableCanvas.setPlayerBet(bet, name);
        }

        @Override
        public void onSomebodyHOS(boolean hos, String name) {
            if(hos) {
                GameroomForm.this.logTextArea.append(name + " hits\n");
            }
            else {
                GameroomForm.this.logTextArea.append(name + " stands\n");
            }
        }

        @Override
        public void onRoundResult(long winning, String name) {
            if(winning == 0) {
                playerInfo.incLoses();
            }
            else {
                playerInfo.setCash(playerInfo.getCash() + winning);
                playerInfo.incWins();
            }
            GameroomForm.this.logTextArea.append(name + " won $" + winning + "\n");
            if(name.equals(playerInfo.getUsername())) {
                JOptionPane.showMessageDialog(GameroomForm.this.frame, "You won $" + winning);
            }

            GameroomForm.this.gameTableCanvas.startNewRound();
        }
    }

    public GameroomForm(GameroomInfo gameroom, final PlayerInfo playerInfo, List<String> players) {
        this.gameroom = gameroom;
        this.playerInfo = playerInfo;

        if(players != null) {
            for(String name : players) {
                gameTableCanvas.addPlayer(name);
            }
        }
        gameTableCanvas.addPlayer(playerInfo.getUsername());

        frame = new JFrame("Gameroom: " + gameroom.getName());
        frame.setContentPane(parentPanel);
        frame.setResizable(false);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                super.windowClosed(e);
                MainForm mainForm = new MainForm(playerInfo);
                mainForm.show();
            }
        });
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
    }

    public void show() {
        frame.setVisible(true);
    }

    public void close() {
        frame.setVisible(false);
        frame.dispose();

        MainForm mainForm = new MainForm(playerInfo);
        mainForm.show();
    }
}
