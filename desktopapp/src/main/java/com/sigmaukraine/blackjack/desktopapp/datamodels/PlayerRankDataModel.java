package com.sigmaukraine.blackjack.desktopapp.datamodels;

import com.sigmaukraine.blackjack.common.msgs.PlayerInfo;

import javax.swing.table.AbstractTableModel;

public class PlayerRankDataModel extends AbstractTableModel {
    private PlayerInfo playerInfo;
    int rank;
    private static final String[] COL_NAMES = {"#", "Username", "Cash", "Wins", "Loses"};

    public PlayerRankDataModel(PlayerInfo playerInfo, int rank) {
        this.playerInfo = playerInfo;
        this.rank = rank;
    }

    @Override
    public int getRowCount() {
        return 1;
    }

    @Override
    public int getColumnCount() {
        return COL_NAMES.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0: return rank;
            case 1: return playerInfo.getUsername();
            case 2: return playerInfo.getCash();
            case 3: return playerInfo.getWins();
            case 4: return playerInfo.getLoses();
            default: return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        return COL_NAMES[column];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
