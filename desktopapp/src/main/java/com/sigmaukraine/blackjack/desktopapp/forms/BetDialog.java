package com.sigmaukraine.blackjack.desktopapp.forms;

import com.sigmaukraine.blackjack.common.msgs.GameroomInfo;
import com.sigmaukraine.blackjack.common.msgs.PlayerInfo;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BetDialog {
    private JDialog dialog;
    private JPanel parentPanel;
    private JSpinner betSpinner;
    private JSlider betSlider;
    private JButton betButton;
    private long result;

    public BetDialog(final JFrame owner, GameroomInfo gameroom, PlayerInfo playerInfo) {
        betSlider.setMinimum((int) gameroom.getMinBet());
        betSlider.setMaximum(playerInfo.getCash().intValue());

        betSpinner.setModel(new SpinnerNumberModel((int)(gameroom.getMinBet()), (int)(gameroom.getMinBet()), playerInfo.getCash().intValue(), 1));

        betSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                betSpinner.setValue(betSlider.getValue());
            }
        });

        betSpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                betSlider.setValue((int)betSpinner.getValue());
            }
        });

        betButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                result = betSlider.getValue();
                close();
            }
        });

        betSlider.setValue((int) gameroom.getMinBet());

        dialog = new JDialog(owner, "Bet", true);
        dialog.setContentPane(parentPanel);
        dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        dialog.pack();
    }

    public long show() {
        dialog.setVisible(true);
        return result;
    }

    private void close() {
        dialog.setVisible(false);
        dialog.dispose();
    }
}
