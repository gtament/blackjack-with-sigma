package com.sigmaukraine.blackjack.desktopapp.forms;

import com.sigmaukraine.blackjack.client.ClientServerManager;
import com.sigmaukraine.blackjack.common.msgs.GameroomInfo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CreateGameroomDialog {
    private JDialog dialog;
    private JPanel parentPanel;
    private JTextField nameTextField;
    private JSpinner betsSpinner;
    private JSpinner playersSpinner;
    private JButton createNewGameroomButton;

    private class CreteNewGameroomActionListener implements ActionListener {

        private MainForm owner;

        public CreteNewGameroomActionListener(MainForm owner) {
            this.owner = owner;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            GameroomInfo newGameroom = new GameroomInfo(nameTextField.getText(),
                    (long)betsSpinner.getValue(),
                    (int)playersSpinner.getValue());
            GameroomForm gameroomForm = new GameroomForm(newGameroom, owner.getPlayerInfo(), null);
            ClientServerManager.getInstance().setGamelogicEventsHandler(gameroomForm.new GamelogicEventsImpl());

            if(ClientServerManager.getInstance().createGameroom(newGameroom)) {
                owner.close();
                CreateGameroomDialog.this.close();

                gameroomForm.show();

                ClientServerManager.getInstance().startRound();
            }
            else {
                JOptionPane.showMessageDialog(parentPanel,
                        "Could not create gameroom.",
                        "Error!",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public CreateGameroomDialog(final MainForm owner) {
        betsSpinner.setModel(new SpinnerNumberModel(Long.valueOf(5L), Long.valueOf(5L), Long.valueOf(10000L), Long.valueOf(5L)));
        playersSpinner.setModel(new SpinnerNumberModel(3, 1, 6, 1));

        createNewGameroomButton.addActionListener(new CreteNewGameroomActionListener(owner));

        dialog = new JDialog(owner.getFrame(), "Create new gameroom", true);
        dialog.setContentPane(parentPanel);
        dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        dialog.pack();
    }

    public void show() {
        dialog.setVisible(true);
    }

    public void close() {
        dialog.setVisible(false);
        dialog.dispose();
    }
}
