package com.sigmaukraine.blackjack.desktopapp;

import com.sigmaukraine.blackjack.common.cards.Card;
import com.sigmaukraine.blackjack.common.cards.Dignity;
import com.sigmaukraine.blackjack.common.cards.Suit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameTableCanvas extends JPanel {

    private  static final  transient Logger LOG = LogManager.getLogger(GameTableCanvas.class.getName());

    private static class PlayerDrawable {
        private String name;
        private long bet;
        private List<Card> cards = new ArrayList<>();
        private List<Boolean> cardsShown = new ArrayList<>();

        public PlayerDrawable(String name) {
            this.name = name;
            this.bet = 0L;
        }

        public String getName() {
            return name;
        }

        public Long getBet() {
            return bet;
        }

        public List<Card> getCards() {
            return cards;
        }

        public List<Boolean> getCardsShown() {
            return cardsShown;
        }

        public void giveCard(Card c, boolean shown) {
            cards.add(c);
            cardsShown.add(shown);
        }

        public void setBet(long bet) {
            this.bet = bet;
        }

        public void clearCards() {
            cards.clear();
            cardsShown.clear();
        }
    }

    private static final int WIDTH = 1000;
    private static final int HEIGHT = 650;
    private static final int CARD_WIDTH = 93;
    private static final int CARD_HEIGHT = 143;

    private static final Point[] POSITIONS = {new Point(30, 30),
                                              new Point(130, 240),
                                              new Point(250, 450),
                                              new Point(580, 450),
                                              new Point(700, 240),
                                              new Point(800, 30)};

    private static final Point DEALER_POSITION = new Point(415, 30);

    private static final Color BACKGROUND_COLOR = new Color(0, 128, 0);
    private static final Color BORDER_COLOR = new Color(0, 0, 0);
    private static final Font FONT = new Font("Arial", Font.BOLD, 16);

    private transient BufferedImage table;
    private transient BufferedImage jacket;
    private transient Map<Card, BufferedImage> cards = new HashMap<>();

    private transient List<PlayerDrawable> players = new ArrayList<>();
    private transient PlayerDrawable dealer = new PlayerDrawable("DEALER");

    public GameTableCanvas() {
        setMinimumSize(new Dimension(WIDTH, HEIGHT));
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setMaximumSize(new Dimension(WIDTH, HEIGHT));
        setBorder(new LineBorder(BORDER_COLOR));
        setBackground(BACKGROUND_COLOR);

        loadGraphics();
    }

    private void loadGraphics() {
        try {
            table = ImageIO.read(getClass().getClassLoader().getResource("table.png"));
            jacket = ImageIO.read(getClass().getClassLoader().getResource("cards/jacket.png"));

            for(int i = 0; i < 4; i++) {
                StringBuilder url = new StringBuilder("cards/");
                Suit suit = null;
                switch (i) {
                    case 0:
                        url.append("spades/");
                        suit = Suit.SPADES;
                        break;
                    case 1:
                        url.append("clubs/");
                        suit = Suit.CLUBS;
                        break;
                    case 2:
                        url.append("hearts/");
                        suit = Suit.HEARTS;
                        break;
                    case 3:
                        url.append("diamonds/");
                        suit = Suit.DIAMONDS;
                        break;
                    default:
                }
                for(int j = 0; j < 13; j++) {
                    switch (j) {
                        case 0:
                            cards.put(new Card(suit, Dignity.TWO), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "2.png")));
                            break;
                        case 1:
                            cards.put(new Card(suit, Dignity.THREE), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "3.png")));
                            break;
                        case 2:
                            cards.put(new Card(suit, Dignity.FOUR), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "4.png")));
                            break;
                        case 3:
                            cards.put(new Card(suit, Dignity.FIVE), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "5.png")));
                            break;
                        case 4:
                            cards.put(new Card(suit, Dignity.SIX), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "6.png")));
                            break;
                        case 5:
                            cards.put(new Card(suit, Dignity.SEVEN), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "7.png")));
                            break;
                        case 6:
                            cards.put(new Card(suit, Dignity.EIGHT), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "8.png")));
                            break;
                        case 7:
                            cards.put(new Card(suit, Dignity.NINE), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "9.png")));
                            break;
                        case 8:
                            cards.put(new Card(suit, Dignity.TEN), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "10.png")));
                            break;
                        case 9:
                            cards.put(new Card(suit, Dignity.JACK), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "jack.png")));
                            break;
                        case 10:
                            cards.put(new Card(suit, Dignity.QUEEN), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "queen.png")));
                            break;
                        case 11:
                            cards.put(new Card(suit, Dignity.KING), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "king.png")));
                            break;
                        case 12:
                            cards.put(new Card(suit, Dignity.ACE), ImageIO.read(getClass().getClassLoader().getResource(url.toString() + "ace.png")));
                            break;
                        default:
                    }
                }
            }
        }
        catch (IOException e) {
            LOG.catching(e);
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawImage(table, 0, 0, WIDTH, HEIGHT, null);
        /*draw players with their cards*/
        for(int i = 0; i < players.size(); i++) {
            PlayerDrawable p = players.get(i);
            for(int j = 0; j < p.getCards().size(); j++) {
                Card c= p.getCards().get(j);
                if(p.getCardsShown().get(j)) {
                    drawCard(c, POSITIONS[i].x + j * 20, POSITIONS[i].y, g);
                }
                else {
                    drawJacket(POSITIONS[i].x + j * 20, POSITIONS[i].y, g);
                }
            }
            drawPlayerName(p.getName(), POSITIONS[i].x, POSITIONS[i].y, g);
            drawPlayerBet(p.bet, POSITIONS[i].x, POSITIONS[i].y, g);
        }
        /*draw dealer with his cards*/
        for(int i = 0; i < dealer.getCards().size(); i++) {
            if(dealer.getCardsShown().get(i)) {
                drawCard(dealer.getCards().get(i), DEALER_POSITION.x + i * 20, DEALER_POSITION.y, g);
            }
            else {
                drawJacket(DEALER_POSITION.x + i * 20, DEALER_POSITION.y, g);
            }
        }
        drawPlayerName(dealer.getName(), DEALER_POSITION.x, DEALER_POSITION.y, g);
    }

    private void drawCard(Card card, int x, int y, Graphics g) {
        g.drawImage(cards.get(card), x, y, CARD_WIDTH, CARD_HEIGHT, null);
    }
    private void drawJacket(int x, int y, Graphics g) {
        g.drawImage(jacket, x, y, CARD_WIDTH, CARD_HEIGHT, null);
    }

    private void drawPlayerName(String name, int x, int y, Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setFont(FONT);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int h = g2d.getFontMetrics(FONT).getHeight();
        g2d.drawString(name, x, y + CARD_HEIGHT + 5 + h);
    }

    private void drawPlayerBet(long bet, int x, int y, Graphics g) {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setFont(FONT);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int h = g2d.getFontMetrics(FONT).getHeight();
        g2d.drawString("BET: $" + bet, x, y + CARD_HEIGHT + h + 10 + h);
    }

    public void addPlayer(String name) {
        players.add(new PlayerDrawable(name));
        repaint();
    }

    public void giveDealerCard(Card c, boolean shown) {
        dealer.giveCard(c, shown);
        repaint();
    }

    public void givePlayerCard(Card c, boolean shown, String name) {
        for(PlayerDrawable p : players) {
            if(p.getName().equals(name)) {
                p.giveCard(c, shown);
                break;
            }
        }
        repaint();
    }

    public void setPlayerBet(long bet, String name) {
        for(PlayerDrawable p : players) {
            if(p.getName().equals(name)) {
                p.setBet(bet);
                break;
            }
        }
        repaint();
    }

    public void openDealerCards() {
        int n = dealer.getCardsShown().size();
        dealer.getCardsShown().clear();
        for (int i = 0; i < n; i++) {
            dealer.getCardsShown().add(true);
        }
        repaint();
    }

    public void startNewRound() {
        for(PlayerDrawable p: players) {
            p.clearCards();
            p.setBet(0);
        }
        dealer.clearCards();
        repaint();
    }
}
