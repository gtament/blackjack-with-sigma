package com.sigmaukraine.blackjack.desktopapp.datamodels;

import com.sigmaukraine.blackjack.common.msgs.GameroomInfo;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class GameroomListDataModel extends AbstractTableModel {

    private transient List<GameroomInfo> gamerooms;
    private static final String[] COL_NAMES = {"Name", "Min bet", "Players", "Max players"};

    public GameroomListDataModel(List<GameroomInfo> gamerooms) {
        this.gamerooms = gamerooms;
    }

    @Override
    public int getRowCount() {
        return gamerooms.size();
    }

    @Override
    public int getColumnCount() {
        return COL_NAMES.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        GameroomInfo info = gamerooms.get(rowIndex);
        switch (columnIndex) {
            case 0: return info.getName();
            case 1: return info.getMinBet();
            case 2: return info.getCurPlayers();
            case 3: return info.getMaxPlayers();
            default: return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        return COL_NAMES[column];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public GameroomInfo getGameroomInfo(int rowIndex) {
        return gamerooms.get(rowIndex);
    }
}
